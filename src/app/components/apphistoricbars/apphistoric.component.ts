import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { DocumentoService } from '../../services/documento.service';
//import * as pluginDataLabels from 'chartjs-plugin-datalabels';


@Component({
	selector: 'app-historic',
	templateUrl: './apphistoric.component.html',
	styleUrls: ['./apphistoric.component.css']
})


export class HistoricComponent {



    public cast_loads:number;
    public cast_sent:number;
    public cast_accepted:number;
    public cast_rejected:number;





    public barChartOptions: ChartOptions = {
        responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] }

};
public barChartLabels: Label[] = ['DTE Loads','Emails'];
public barChartType: ChartType = 'bar';
public barChartLegend = true;
  //public barChartPlugins = [pluginDataLabels];

  public lineChartColors: Color[] = [
    { // Light blue - DTE Cargados
        backgroundColor: '#03befc',
        borderColor: '#03adfc',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
        backgroundColor: '#119aa6',
        borderColor: '#119aa6',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
        backgroundColor: 'lightgreen',
        borderColor: 'lightgreen',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // red
        backgroundColor: '#db2323',
        borderColor: '#db2323',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
    ];





    public barChartData: ChartDataSets[] = [            
    { data: [0, 0, 0, 0], label: 'Online' },
    { data: [0, 0, 0, 0], label: 'Online' },
    { data: [0, 0, 0, 0], label: 'Online' },
    { data: [0, 0, 0, 0], label: 'Online' }
    ];




  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);

  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);

  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
    Math.round(Math.random() * 100),
    59,
    80,
    (Math.random() * 100),
    56,
    (Math.random() * 100),
    40];
    this.barChartData[0].data = data;
}



constructor(private _DocumentoService:DocumentoService) {   
}

ngOnInit() {


    this.initBarsLive();
    this.refreshBarsLive();
}


refreshBarsLive(){

    setInterval( ()=> {     
        this._DocumentoService.cast_loads.subscribe(loads=> this.cast_loads = loads);
            //console.log(this.cast_sent);
            this.barChartData = [                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
            { data: [this.cast_loads, 0, 0, 0], label: 'Cargados' },
            { data: [this.cast_sent, 0, 0, 0], label: 'Enviados DIAN' },
            { data: [this.cast_accepted, 0, 0, 0], label: 'Aceptados' },
            { data: [this.cast_rejected, 0, 0, 0], label: 'Rechazados' }
            ]
        },60000);


}

initBarsLive(){

    setTimeout( ()=> {      
        this._DocumentoService.cast_loads.subscribe(res=> this.cast_loads = res);
        this._DocumentoService.cast_sent.subscribe(res=> this.cast_sent = res);
        this._DocumentoService.cast_accepted.subscribe(res=> this.cast_accepted = res);
        this._DocumentoService.cast_rejected.subscribe(res=> this.cast_rejected = res);

        this.barChartData = [           
        { data: [this.cast_loads, 0, 0, 0], label: 'Cargados' },
        { data: [this.cast_sent, 0, 0, 0], label: 'Enviados DIAN' },
        { data: [this.cast_accepted, 0, 0, 0], label: 'Aceptados' },
        { data: [this.cast_rejected, 0, 0, 0], label: 'Rechazados' }
        ]
    },3000);


}



}




import { Component, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {ProgressBarModule} from "angular-progress-bar";
import { DocumentoService } from '../../services/documento.service';
import { Documento } from '../../models/documento';
import { GLOBAL } from '../../services/global';
import { SidebarModule } from 'ng-sidebar';

@Component({
  selector: 'app-appdashboard',
  templateUrl: './appdashboard.component.html',
  styleUrls: ['./appdashboard.component.css']
})
export class AppdashboardComponent {
		public dte_loaded_count: number;
		public dte_response_count: number;
		public total_mail_sent: number;			
		public total_mail_err: number;
		public total_mail_sending;				
		public total_mail_nocomplete;
		public dte_uploaded_count;
		public dte_accepted_count;
		public dte_rejected_count;		
		public dte_responses_dian;
		public total_mail_delivered;
		public dte_loaded_count_porc;	
		public total_mail_sent_porc;
		public total_mail_delivered_porc;
		public total_mail_nocomplete_porc;
		public total_mail_sending_porc;
		public total_mail_err_porc;
		public dte_loaded_porc;
		public dte_responses_porc;		
		public sent_status;
		public last_load;
		public last_upload
		public last_resp_DIAN
		public last_sent_email;
		public mail_ini;
		public mail_pnd;
		public mail_err;
		public total_qcli;
		public q_epr;
		public q_ini;
		public q_val;
		public q_exv;
		public q_dne;
		public q_pnd;
		public total_envi_q;
		public q_ini_porc;
		public q_epr_porc;
		public q_val_porc;
		public q_dne_porc;
		public q_pnd_porc;
		public q_exv_porc;
		public qcli_ini_porc;
		public qcli_pnd_porc;
		public qcli_err_porc;


	constructor(
		private _documentoService: DocumentoService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		//this.dte_loaded_count=0;			
		this.dte_loaded_count=0;
		this.dte_uploaded_count=0;
		this.dte_accepted_count=0;
		this.dte_rejected_count=0;
		this.dte_responses_dian=0;
		this.total_mail_sent=0;
		this.total_mail_delivered=0
		this.total_mail_nocomplete=0;
		this.total_mail_sending=0
		this.total_mail_err=0;
		this.last_load='';
		this.last_upload=''
		this.last_resp_DIAN=''
		this.last_sent_email='';
		this.mail_ini=0;
		this.mail_pnd=0;
		this.mail_err=0;
		this.total_qcli=0;
		this.q_epr=0;
		this.q_ini=0;
		this.q_val=0;
		this.q_exv=0;
		this.q_dne=0;
		this.q_pnd=0;
		this.total_envi_q=0;
		this.q_ini_porc=0;
		this.q_epr_porc=0;
		this.q_val_porc=0;
		this.q_dne_porc=0;
		this.q_pnd_porc=0;
		this.q_exv_porc=0;
		this.qcli_ini_porc=0;
		this.qcli_pnd_porc=0;
		this.qcli_err_porc=0;


	}

	ngOnInit(){
		
		this.showDTECount();
		this.showDTEUploadedCount();
		this.showDTEResponses();
		this.showSentMailStatus();
		this.showLastLoad();
		this.showLastUpload();
		this.showLastRespDIAN();
		this.showLastSentMail();
		this.showQcliStatus();
		this.showEnviQStatus();

		setTimeout(()=>{			
			this.refreshDashboard();				
		},60000);

	}

	showDTECount(){		
		this._documentoService.getDTECount().subscribe(
			response => {
				if(response['code']== 200){	
					this.dte_loaded_count=(response['data']['CARGADOS_SUITE']);
				}

			},
			error => {
				console.log(<any>error);
			}
		);		
	}

	showDTEUploadedCount(){		
		this._documentoService.getDTEUploadedDIAN().subscribe(
			response => {
				if(response['code']== 200){	
					this.dte_uploaded_count=(response['data']['UPLOADED_COUNT']);
				}

			},
			error => {
				console.log(<any>error);
			}
		);		
	}

							

	showLastLoad(){		
		this._documentoService.getLastLoad().subscribe(
			response => {				
				if(response['code']== 200){				
					this.last_load=(response['data'][0]['LAST_LOAD']);		
				}

			},
			error => {
				console.log(<any>error);
				console.log("error");			
			}
		);		
	}

	showLastUpload(){		
		this._documentoService.getLastUpload().subscribe(
			response => {				
				if(response['code']== 200){				
					this.last_upload=(response['data'][0]['LAST_UPLOAD']);		
				}

			},
			error => {
				console.log(<any>error);
						
			}
		);		
	}

	showLastRespDIAN(){		
		this._documentoService.getLastRespDIAN().subscribe(
			response => {				
				if(response['code']== 200){				
					this.last_resp_DIAN=(response['data'][0]['LAST_RESP']);		
				}

			},
			error => {
				console.log(<any>error);
						
			}
		);		
	}

	showLastSentMail(){		
		this._documentoService.getLastSentMail().subscribe(
			response => {				
				if(response['code']== 200){				
					this.last_sent_email=(response['data'][0]['LAST_SENT_MAIL']);		
				}

			},
			error => {
				console.log(<any>error);
						
			}
		);		
	}


	showQcliStatus(){		
		this._documentoService.getQcliStatus().subscribe(
			response => {				
				if(response['code']== 200){				
					this.mail_ini=0;
					this.mail_pnd=0;
					this.mail_err=0;
					this.total_qcli=0;
					this.qcli_ini_porc=0;
					this.qcli_pnd_porc=0;
					this.qcli_err_porc=0;
					for (let i=0;i<response['data'].length;i++){						
						switch (response['data'][i]['ESTADO']){							
							case "ERR": this.mail_err=response['data'][i]['TOTAL'];
							break;							
							case "INI": this.mail_ini=response['data'][i]['TOTAL'];							
							break;													
							case "PND": this.mail_pnd=response['data'][i]['TOTAL'];
							break;
						}
					}
					this.total_qcli=(this.mail_ini+this.mail_pnd+this.mail_err);
					this.qcli_ini_porc=Math.round((((this.mail_ini/this.total_qcli)*100)))+'%';
					this.qcli_pnd_porc=Math.round((((this.mail_pnd/this.total_qcli)*100)))+'%';
					this.qcli_err_porc=Math.round((((this.mail_err/this.total_qcli)*100)))+'%';
				}

			},
			error => {
				console.log(<any>error);

			}
			);		
	}

	showEnviQStatus(){		
		this._documentoService.getEnviQStatus().subscribe(
			response => {				
				if(response['code']== 200){						
					this.q_epr=0;
					this.q_ini=0;
					this.q_val=0;
					this.q_exv=0;
					this.q_dne=0;
					this.q_pnd=0;
					this.q_ini_porc=0;
					this.q_epr_porc=0;				
					this.q_val_porc=0;
					this.q_exv_porc=0;
					this.q_dne_porc=0;
					this.q_pnd_porc=0;
					this.total_envi_q=0;
					for (let i=0;i<response['data'].length;i++){						
						switch (response['data'][i]['ESTADO']){							
							case "INI": this.q_ini=response['data'][i]['TOTAL'];																
							break;							
							case "EPR": this.q_epr=response['data'][i]['TOTAL'];							
							break;													
							case "VAL": this.q_val=response['data'][i]['TOTAL'];
							break;
							case "EXV": this.q_exv=response['data'][i]['TOTAL'];							
							break;													
							case "DNE": this.q_dne=response['data'][i]['TOTAL'];
							break;
							case "PND": this.q_pnd=response['data'][i]['TOTAL'];
							break;							
						}																
					}
					this.total_envi_q=(this.q_ini+this.q_epr+this.q_val+this.q_pnd+this.q_dne+this.q_exv)
					this.q_ini_porc=Math.round((((this.q_ini/this.total_envi_q)*100)))+'%';
					this.q_epr_porc=Math.round((((this.q_epr/this.total_envi_q)*100)))+'%';
					this.q_val_porc=Math.round((((this.q_val/this.total_envi_q)*100)))+'%';
					this.q_dne_porc=Math.round((((this.q_dne/this.total_envi_q)*100)))+'%';
					this.q_pnd_porc=Math.round((((this.q_pnd/this.total_envi_q)*100)))+'%';
					this.q_exv_porc=Math.round((((this.q_exv/this.total_envi_q)*100)))+'%';
				}

			},
			error => {
				console.log(<any>error);

			}
			);		
	}


	showSentMailStatus(){		
		this._documentoService.getSentMailStatus().subscribe(
			response => {
				if(response['code']== 200){
					this.total_mail_sent=0;
					for (let i=0;i<response['data'].length;i++){
						switch (response['data'][i]['ESTADO']){
							case "Evento Aceptado": this.total_mail_delivered=response['data'][i]['TOTAL'];							
								break;													
							case "Evento en Ejecucion": this.total_mail_sending=response['data'][i]['TOTAL'];
								break;
							case "Evento Incompleto": this.total_mail_nocomplete=response['data'][i]['TOTAL'];
								break;
							case "Evento Rechazado": this.total_mail_err=response['data'][i]['TOTAL'];
								break;						
						}
					}
					this.total_mail_sent=this.total_mail_delivered+this.total_mail_sending+this.total_mail_err+this.total_mail_nocomplete;
					this.total_mail_delivered_porc=Math.round((((this.total_mail_delivered/this.total_mail_sent)*100)))+'%';
					this.total_mail_sending_porc=Math.round(((this.total_mail_sending/this.total_mail_sent))*100)+'%';
					this.total_mail_nocomplete_porc=Math.round((this.total_mail_nocomplete/this.total_mail_sent)*100)+'%';
					this.total_mail_err_porc=Math.round((((this.total_mail_err/this.total_mail_sent))*100))+'%';

				}else{												
					this.initMailCount();
				}
			},
			error => {
				console.log(<any>error);
			}
		);		
	}

	public showDTEResponses(){		
		this._documentoService.getDTEResponseDIAN().subscribe(
			response => {
				if(response['code']== 200){	
					if (response['data'].length==1){
						if(response['data'][0]['ESTADO'] = 'Aceptado por DIAN'){
							this.dte_accepted_count = response['data'][0]['TOTAL'];
							this.dte_rejected_count = 0;														
						}else{							
							this.dte_rejected_count=response['data'][0]['TOTAL'];
							this.dte_accepted_count = 0;										
						}	
					}else{
						if(response['data'][0]['ESTADO'] = 'Aceptado por DIAN'){
							this.dte_accepted_count = response['data'][0]['TOTAL'];
							this.dte_rejected_count=response['data'][1]['TOTAL'];					
						}else{
							this.dte_rejected_count=response['data'][0]['TOTAL'];
							this.dte_accepted_count=response['data'][1]['TOTAL'];
						}

					}
				}else{	
					if(response['code']== 404){	
						this.dte_accepted_count=0;
						this.dte_rejected_count=0;
					}
					
				}
			},
			error => {
				console.log(<any>error);
			}
		);		
	}


	showProgressbar(){		
			this.dte_loaded_porc=Math.round((((this.dte_uploaded_dian/this.dte_loaded_count)*100)))+'%';
			this.dte_responses_porc=Math.round(((this.dte_rejected_count+this.dte_accepted_count)/this.dte_uploaded_dian)*100)+'%';
			this.total_mail_sent_porc=Math.round((this.total_mail_sent/this.dte_accepted_count)*100)+'%';
			this.total_mail_delivered_porc=Math.round((((this.total_mail_delivered+this.total_mail_nocomplete)/this.total_mail_sent)*100))+'%';	
	};


	initMailCount(){
		this.total_mail_delivered=0;					
		this.total_mail_sending=0;
		this.total_mail_nocomplete=0;
		this.total_mail_err=0;
		this.total_mail_sent=0;
	}

	refreshDashboard(){
		setInterval( ()=> {
			this.showDTECount();
			this.showDTEUploadedCount();
			this.showDTEResponses();
			this.showSentMailStatus();
			this.showProgressbar();	
			this.showLastLoad();
			this.showLastUpload();
			this.showLastRespDIAN();
			this.showLastSentMail();
			this.showQcliStatus();
			this.showEnviQStatus();

		}, 60000);

	};



}


		

		

import { Component } from '@angular/core';

@Component({
	 selector: 'error',
	 templateUrl: '../views/error.html'	
})

export class ErrorComponent{
	public titulo: string;

	constructor(){
		this.titulo = 'Error al cargar la pagina!';		
	}

	ngOnInit(){
		console.log("Se ha cargado la pagina de error");
	}
}

import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DocumentoService } from '../services/documento.service';
import { Documento } from '../models/documento';
import { GLOBAL } from '../services/global';

@Component({
	selector: 'envio-adquiriente',
	templateUrl: '../views/envio-adquiriente.html',
	providers: [DocumentoService]
})
export class EnvioAdquirienteComponent{
	public titulo: string;
	public total: number;
	public documento: Documento;
	public no_events: number;
	public listado_envios: Array<any>;


	constructor(
		private _documentoService: DocumentoService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		this.titulo = 'Busqueda';
		this.total = 0;
		this.no_events=0;
		this.listado_envios = [];
		this.documento = new Documento(0,0,0,'','','','','','','');
	
	}

	ngOnInit(){
		
	}

	onSubmit(){
		this.total=0;
		this.listado_envios = [];		
		this.showMailList();	

	}

	showMailList(){		
		this._documentoService.getMailList(this.documento.iden_emis,this.documento.iden_docu).subscribe(
			response => {
				if(response['code']== 200){											
					this.listado_envios=(response['data']);	
					this.total=response['data'].length;				
					this.no_events=0;
						//this._router.navigate(['/listado-envios']);
				}else{							
					this.listado_envios = [];							
					this.no_events=1;
				}
			},
			error => {
				console.log(<any>error);
			}


		);
	}

	clearControls(){

		this.documento.iden_emis=0;
		this.documento.iden_docu='';
		this.no_events=0;

	}
}
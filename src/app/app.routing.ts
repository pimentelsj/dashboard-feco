// componentes de angular necesarios
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// importar componentes
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { EnvioAdquirienteComponent } from './components/envio-adquiriente.component';
import { AppdashboardComponent } from './components/appdashboard/appdashboard.component';
 
const appRoutes: Routes = [
	
	{ path: '', component: AppdashboardComponent },
	{ path: 'index', component: AppdashboardComponent },
	{ path: 'home', component: AppdashboardComponent },
	{ path: 'dashboard', component: AppdashboardComponent },
	{ path: '**', component: ErrorComponent }
];	

export const appRoutingProviders:any [] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);


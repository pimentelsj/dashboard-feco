import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ProgressBarModule} from "angular-progress-bar";
import { ChartsModule } from 'ng2-charts';
import { DocumentoService } from './services/documento.service';

import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';



import { AppRoutingModule } from './app-routing.module';
import { AppheaderComponent } from './components/appheader/appheader.component';
import { AppmenuComponent } from './components/appmenu/appmenu.component';
import { AppfooterComponent } from './components/appfooter/appfooter.component';
import { AppsettingComponent } from './components/appsetting/appsetting.component';
import { BarComponent } from './components/appbar/appbar.component';
import { HistoricComponent } from './components/apphistoricbars/apphistoric.component';
import { LineaComponent } from './components/applinea/applinea.component';
import { AppdashboardComponent } from './components/appdashboard/appdashboard.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    AppheaderComponent,
    AppmenuComponent,
    AppfooterComponent,
    AppsettingComponent,
    BarComponent,
    LineaComponent,
    AppdashboardComponent,
    HistoricComponent
      


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    routing,
    ChartsModule
  ],

  providers: [appRoutingProviders,DocumentoService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Producto } from '../models/producto';
import { GLOBAL } from './global';

@Injectable()
export class ProductoService{
	public url: string;

	constructor(
		public _http: HttpClient
	){
		this.url = GLOBAL.url;
	}

	getProductos(){
		return this._http.get(this.url+'/productos').pipe(map(res => res));
	}

	getProducto(id){
		return this._http.get(this.url+'/producto/'+id).pipe(map(res => res));
	}

	addProducto(producto: Producto){
		let json = JSON.stringify(producto);
		let params = 'json='+json;
		//let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/productos', params, {headers : new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'})})
						 .pipe(map(res => res));
	}


	editProducto(id, producto: Producto){
		let json = JSON.stringify(producto);
		let params = "json="+json;
		//let headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/update-producto/'+id, params, {headers: new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'})})
						 .pipe(map(res => res));
	}

	deleteProducto(id){
		return this._http.get(this.url+'/delete-producto/'+id)
						 .pipe(map(res => res));
	}

	makeFileRequest(url: string, params: Array<string>, files: Array<File>){
		return new Promise((resolve, reject)=>{
			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();
			xhr.responseType ="json" ;

		//	for(var i = 0; i < files.length; i++){
				
				formData.append('uploads[]', files[0], files[0].name);
		//	}

			
			
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4){
					if(xhr.status == 200){																		
						resolve(xhr.response);
					}else{
						reject(xhr.response);
					}
				}
			};

			xhr.open("POST", url, true);
			xhr.send(formData);
		});
	}

}
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable,BehaviorSubject } from 'rxjs';

import { Documento } from '../models/documento';
import { GLOBAL } from './global';

@Injectable()
export class DocumentoService{
	public url: string;

	private loads = new BehaviorSubject<number>(0);
	private sent  = new BehaviorSubject<number>(0);
	private accepted = new BehaviorSubject<number>(0);
	private rejected = new BehaviorSubject<number>(0);


    cast_loads = this.loads.asObservable();
    cast_sent  = this.sent.asObservable();
    cast_accepted = this.accepted.asObservable();
    cast_rejected = this.rejected.asObservable();    
    


	constructor(
		public _http: HttpClient
	){
		this.url = GLOBAL.url;		
	}

	
	getMailList(emisor, folio){

		return this._http.get(this.url+'/envio-adquiriente/'+emisor+'/'+folio).pipe(map(res => res));
	}

	getDTECount(){
		return this._http.get(this.url+'/dte-loaded').pipe(map(res => {			
				
				if(res['code']== 200){	
					this.loads.next(res['data']['CARGADOS_SUITE']);										
				}

			return res;
		}));
	}
	

	getDTEUploadedDIAN(){
		return this._http.get(this.url+'/dte-uploaded-dian').pipe(map(res => {			
				
				if(res['code']== 200){									
					this.sent.next(res['data']['UPLOADED_COUNT']);
				}

			return res;
		}));
	}

	getLastLoad(){
		return this._http.get(this.url+'/last-load').pipe(map(res => res));
	}
	
	getLastUpload(){
		return this._http.get(this.url+'/last-upload').pipe(map(res => res));
	}
	

	getLastRespDIAN(){
		return this._http.get(this.url+'/last-dian-resp').pipe(map(res => res));
	}

	getLastSentMail(){
		return this._http.get(this.url+'/last-sent_mail').pipe(map(res => res));
	}

	getQcliStatus(){
		return this._http.get(this.url+'/qcli-status').pipe(map(res => res));
	}

	getEnviQStatus(){
		return this._http.get(this.url+'/envi-q-status').pipe(map(res => res));
	}



	getDTEResponseDIAN(){
		return this._http.get(this.url+'/with-response-dian').pipe(map(res => {
			
				if(res['code']== 200){	
					if (res['data'].length==1){
						if(res['data'][0]['ESTADO'] = 'Aceptado por DIAN'){
							this.accepted.next(res['data'][0]['TOTAL']);
							this.rejected.next(0);														
						}else{							
							this.rejected.next(res['data'][0]['TOTAL']);
							this.accepted.next(0);										
						}	
					}else{
						if(res['data'][0]['ESTADO'] = 'Aceptado por DIAN'){
							this.accepted.next(res['data'][0]['TOTAL']);
							this.rejected.next(res['data'][1]['TOTAL']);					
						}else{
							this.rejected.next(res['data'][0]['TOTAL']);
							this.accepted.next(res['data'][1]['TOTAL']);
						}
					}
				}

			return res;
		}));
	}
	
	

	getSentMailStatus(){
		return this._http.get(this.url+'/sent-mail-status').pipe(map(res => res));
	}

	
	
	
	
}
export class Producto{	

	constructor(
		public id: string,
		public nombre: string,
		public precio: number,
		public descripcion: string,
		public imagen: string  
	){}
}
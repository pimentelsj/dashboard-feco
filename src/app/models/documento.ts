export class Documento{	

	constructor(
		public iden_emis: number,
		public iden_rece: number,
		public nume_docu: number,
		public iden_docu: string,
		public pref_docu: string,
		public fech_emis: string,  
		public fech_carg: string,
		public deli_esta: string,
		public ptfc_msg: string,
		public qadd_mail: string,  		  

	){}
}